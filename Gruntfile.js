module.exports = function (grunt) {
	// load all grunt tasks
	require("load-grunt-tasks")(grunt);

	grunt.initConfig({
		autoprefixer: {
			options: {
				// Task-specific options go here.
				browsers: ["last 2 versions", "ie 8", "ie 9"]
			},
			single_file: {
				options: {
				// Target-specific options go here.
				},
				src: "htdocs/styles/main.css",
				dest: "htdocs/styles/main.css"
			}
		},

		sass: {                              
			dist: {                            
				options: {                       
					style: "expanded"
				},
				files: {
					// 'destination': 'source'
					"htdocs/styles/main.css": "htdocs/styles/scss/main.scss"
				}
			}
		},

		// Run predefined tasks whenever watched file patterns are added, changed or deleted.
		// https://github.com/gruntjs/grunt-contrib-watch
		watch: {
			options: {
			},
			css: {
				files: "htdocs/styles/scss/{,*/}*.scss",
				tasks: ["sass", "autoprefixer"]
			}
		},

		browserSync: {
			dev: {
				bsFiles: {
					src: [
						"htdocs/styles/*.css",
						"htdocs/assets/images/**/*.jpg",
						"htdocs/assets/images/**/*.png",
						"htdocs/scripts/**/*.js",
						"htdocs/**/*.html"
					]
				},
				options: {
					watchTask: true,
					proxy: "local.ra.com"
				}
			}
		},

		clean: {
			staging: [
				"staging/*",
				".tmp/*"
			]
		},

		copy: {
			staging: {
				files: [{
					expand: true,
					dot: true,
					cwd: "htdocs",
					dest: "staging",
					src: [
						"*.{ico,txt}",
						".htaccess",
						"index.html",
						"assets{,*/}**"
					]
				}]
			}
		},

		// useminPrepare Tells usemin which HTML file to use for instructions.
		// This will look for our <!-- build --> comments in index.html
		// included with grunt-usemin
		// https://github.com/yeoman/grunt-usemin
		useminPrepare: {
			html: "htdocs/index.html",
			options: {
				dest: "staging"
			}
		},

		// Minify stylesheet. Move minified css to staging.
		// https://github.com/gruntjs/grunt-contrib-cssmin
		cssmin: {
			staging: {
				files: {
					"staging/styles/main.css": [
						"htdocs/styles/{,*/}*.css"
					]
				}
			}
		},

		// Static file asset revisioning through content hashing
		// https://github.com/cbas/grunt-rev
		rev: {
			staging: {
				files: {
					src: [
						"staging/styles/{,*/}*.css",
						"staging/assets/images/{,*/}*.{png,jpg,jpeg,gif,webp}",
						"staging/scripts/*.*"
					]
				}
			}
		},

		// Replaces references to non-optimized scripts or stylesheets into a set of HTML files (or any templates/views).
		// use <!-- build --> comments for rules
		// https://github.com/yeoman/grunt-usemin
		usemin: {
			html: ["staging/{,*/}*.html"],
			css: ["staging/styles/*.css"],
			options: {
				dirs: ["staging"]
			}
		},

		// used in grunt staging task to FTP staging directory using credentials found in .ftppass
		// https://github.com/inossidabile/grunt-ftpush
		ftpush: {
			staging: {
				auth: {
					host: "205.186.150.23",
					port: 21,
					authKey: "key1"
				},
				src: "htdocs",
				dest: "renovateamerica.billkeller.name/",
				exclusions: [
					"{,*/}.DS_Store",
					"styles/scss/{,*/}.scss",
					"styles/main.css.map"
				]
			}
		}


	});

	// Call watch task AFTER browsersync
	grunt.registerTask("default", [
		// "browserSync",
		"watch"
	]);

	// grunt build
	// builds a staging website, locally only
	grunt.registerTask("build", [
		"clean:staging",
		"copy:staging",
		"useminPrepare",
		"cssmin",
		"concat",
		"uglify",
		"rev",
		"usemin"
	]);

};
