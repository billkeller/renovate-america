var RENO = RENO || {};

RENO.scroll = function(location){
	// get height of header
	var headerHeight = $(".lay-header").outerHeight(true);
	$("html,body").animate({
		scrollTop: $(location).offset().top - headerHeight
	}, 1500);
};

RENO.placeholders = function(){
	$("input, textarea").placeholder({
		customClass: "ie-placeholder"
	});
};

RENO.overlays = function(){
	if ($(window).width() > 768) {
		var width = "50%";
	} else {
		var width = "90%";
	}
	
	$(".js-colorbox").colorbox({
		inline: true,
		href: $(this).attr("href"),
		width: width
	});
};

RENO.navMobilePush = function() {
	// http://tympanus.net/codrops/2013/04/17/slide-and-push-menus/
	var menuRight = $("#js-nav-push"),
		body = $("body"),
		header = $(".lay-header");

	$("#showRightPush").on("click", function(e){
		e.preventDefault();
		body.toggleClass("nav-push-toleft");
		header.toggleClass("nav-push-toleft");
		menuRight.toggleClass("nav-push-open");
	});
};

RENO.videoScaling = function(){
	$(".video-wrap").fitVids();
};

RENO.mediaClick = function(){
	// on click of .list-media > li, show .media-content
	$(".list-media > li").on("click", function(){
		// hide all others
		$(this).find(".media-content").removeClass("is-current");
		$(this).find(".media-content").addClass("is-current");
	});
};

RENO.slideshows = {
	// Careers page: Renovate America in the Media
	media: function(){
		if ($(".js-flexslider-media").length) {
			$(".js-flexslider-media").flexslider({
				autoPlay: true,
				animation: "slide",
				animationLoop: true,
				itemMargin: 5,
				pausePlay: false,
				directionNav: false,
				controlNav: true,
				selector: ".list-media > li"
			});
		}
	},

	mediaDestroy: function() {
		if ($(".js-flexslider-media").length) {
			$(".js-flexslider-media").flexslider("destroy");
		}
	},

	homeStats: function(){
		var t1 = function() {
			$("#stat-one").fadeOut(750);
			$("#stat-two").fadeOut(1000);
			$("#stat-three").fadeOut(1250);
		};
		var t2 = function() {
			$("#stat-four").fadeIn(750);
			$("#stat-five").fadeIn(1000);
			$("#stat-six").fadeIn(1250);
		};
		var t3 = function() {
			$("#stat-four").fadeOut(750);
			$("#stat-five").fadeOut(1000);
			$("#stat-six").fadeOut(1250);  
		};
		var t4 = function() {
			$("#stat-one").fadeIn(750);
			$("#stat-two").fadeIn(1000);
			$("#stat-three").fadeIn(1250);
		}
		setInterval(function(){
			setTimeout(t1, 1000)
			setTimeout(t2, 2000)
			setTimeout(t3, 10000)
			setTimeout(t4, 11000)
		}, 16000);
	}
};

RENO.contentReveal = {
	attach: function() {
		// on click of team category, show team below (mobile only)
		$(".js-team-reveal").on("click", function(){
			var $this = $(this),
				team = $this.next(".list-team");

			if ($this.hasClass("is-open")) {
				team.slideUp(function(){
					$this.removeClass("is-open");
					team.removeClass("is-open");

				});
			} else {
				$this.next(".list-team").slideDown();
				$this.addClass("is-open");
				team.addClass("is-open");
			}
		});

		// on click of the word Archives on the news page,
		// show the archive list below (mobile only)
		$(".js-archive-reveal").on("click", function(){
			var $this = $(this),
				team = $this.next(".list-archives");

			if ($this.hasClass("is-open")) {
				team.slideUp(function(){
					$this.removeClass("is-open");
					team.removeClass("is-open");

				});
			} else {
				$this.next(".list-archives").slideDown();
				$this.addClass("is-open");
				team.addClass("is-open");
			}
		});
	},

	unAttach: function(){
		$(".js-team-reveal").off();
		$(".js-archive-reveal").off();
		// make sure our lists are open
		$(".list-team").removeAttr("style");
		$(".list-archives").removeAttr("style");
	}
};

var queries = [
	{
		context: "mobile",
		match: function() {
			console.log("Matched Mobile");
			// set width of header
			// RENO.fixedHeader.setWidth();
			// Setup media slideshow (careers page)
			RENO.slideshows.media();
			// attach click events for team
			RENO.contentReveal.attach();
		},
		unmatch: function() {
			console.log("leaving mobile!");
			// Destory media slideshow
			RENO.slideshows.mediaDestroy();
		}
	},
	{
		context: "desktop",
		match: function() {
			console.log("Matched Desktop");
			// remove click events on our mobile only reveals
			RENO.contentReveal.unAttach();
			// Run the homepage stats slideshow (fade in/out)
			RENO.slideshows.homeStats();
			
		},
		unmatch: function() {
			console.log("leaving Desktop!");
		}

	}
];

RENO.init = function(){
	RENO.navMobilePush();
	RENO.placeholders();
	RENO.overlays();
	RENO.videoScaling();
	RENO.mediaClick();
	MQ.init(queries);
};

$(document).ready(function(){
	RENO.init();

	// generic scrollTo
	$(".js-scroll").on("click", function(e){
		e.preventDefault();
		var location = $(this).attr("href");
		RENO.scroll(location);	
	});

	$(".js-flexslider-careers").flexslider({
		autoPlay: true,
		animation: "slide",
		animationLoop: true,
		itemMargin: 5,
		pausePlay: false,
		directionNav: true,
		controlNav: true,
		nextText: "",
		prevText: "",
		smoothHeight: false
	});

	$(".js-flexslider-cta-energy").flexslider({
		autoPlay: true,
		animation: "slide",
		animationLoop: true,
		itemMargin: 5,
		pausePlay: false,
		directionNav: false,
		controlNav: true,
		smoothHeight: true
	});

	// form validation on homepage contact form
	$(".form-contact").validate({
		debug: true,
		onfocusout: false,
		errorClass: "is-error",
		rules: {
			"first-name": {
				required: true
			},
			"last-name": {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			message: {
				required: true
			},
			phone: {
				phoneUS: true
			}
		},
		messages: {
			"first-name": {
				required: "First Name is required"
			},
			"last-name": {
				required: "Last Name is required"
			},
			email: {
				required: "Email is required"
			},
			phone: {
				phoneUS: "Please use valid phone #"
			},
			message: {
				required: "Your Message is required"
			}
		},
		errorPlacement: function(error, element) {
			// console.log("error: ", error);
			// console.log("element: ", element);
			// $(element).attr("placeholder", error[0].innerText);

			// if phone, insert after EXT field
			if ($(element).attr("type") === "tel") {
				$(element).closest("div").append(error);
			} else {
				error.insertAfter(element);
			}

		},
		highlight: function(element, errorClass) {
			$(element).addClass(errorClass);
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},

		// Sample responses:
		// {"error":1,"messages":["Sorry, that email and password combination was not recognized."]}
		// {"success":1,"data":{"url": "http://dev.mac.com/account"}}
		submitHandler: function(form) {
			$.ajax({
				type: "POST",
				url: $(form).attr("action"),
				data: $(form).serialize(),
				dataType: "json",
				success: function(response) {
					if (response.success) {
						// server responds with success
					} else {
						// server responds with error
					}
				},
				// server side error, NO response
				error: function(response){
					console.group("server Side Error");
					console.log("ResponseText: ", response.responseText);
					console.log("Full Response: ", response);
					console.groupEnd();
				}
			});
			return false;
		}
	});

});
